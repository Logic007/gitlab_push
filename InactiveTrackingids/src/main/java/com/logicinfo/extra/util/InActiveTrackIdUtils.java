package com.logicinfo.extra.util;

public class InActiveTrackIdUtils {

	public static final String executeprocedure = "{call INACTIVE_TRACKING_ECOMM_ORDERS(?,?)}";
	public static final String fetchrecordsfromsim = "select DISTINCT cust_order_no, tracking_id , ship_carrier_id ,PROCESS_IND from INACTIVE_ORDER_TRACKING_STATUS WHERE PROCESS_IND=?";
	public static final String fetchrecordsbasedonid = "SELECT ID,CODE, DESCRIPTION, MANIFEST_TYPE FROM shipment_carrier";
	public static final String fetchwmsallinactivetrackingrecords = "select CUST_ORDER_NBR, CARRIER_SHIPMENT_NBR, CARRIER_CODE from xx_awb_status_upload where UPLOAD_STATUS=? and EVENT_CODE=?";
	public static final String updatesimheadertable = "UPDATE INACTIVE_ORDER_TRACKING_STATUS SET PROCESS_IND='Y' , LAST_UPDATETIME=SYSDATE WHERE CUST_ORDER_NO= ? AND TRACKING_ID= ?";
	public static final String updatewmsheadertable = "UPDATE  xx_awb_status_upload SET LAST_MODIFIED_DATE=SYSDATE , UPLOAD_STATUS ='Y' , MODIFIED_BY ='OMSUSER' WHERE EVENT_CODE='Cancel' AND UPLOAD_STATUS='N'AND CUST_ORDER_NBR= ? AND CARRIER_SHIPMENT_NBR= ? AND CARRIER_NAME= ?";

}
